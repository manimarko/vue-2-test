import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import {searchPackages} from "../plugins/search";
import {router} from "../route";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        query: "",
        page: 1,
        pagesCount: 0,
        loading: false,

        packages: [],
    },
    mutations: {
        setQuery(state, query) {
            state.query = query;
        },
        setPackages(state, packages) {
            state.packages = packages;
        },
        setLoading(state, loading) {
            state.loading = loading;
        },
        setPage(state, page) {
            state.page = page;
        },
        setPagesCount(state, count) {
            state.pagesCount = count;
        },
    },
    actions: {
        searchPackagesDebounce: _.debounce(function (context, query = undefined) {
            //reset page to 1 because of new query
            context.dispatch("searchPackages", {query: query, page: 1});
        }, 500),
        searchPackages: async function (context, data = undefined) {
            const newQuery = data ? data.query : "";
            const newPage = data ? data.page : 1;

            if (newQuery) {
                context.commit("setQuery", newQuery);
            } else {
                context.commit("setQuery", newQuery);
                context.commit("setPackages", []);
                context.commit("setPagesCount", 0);
                if (router.currentRoute.name !== "home") {
                    router.push({
                        name: 'home',
                        params: {}
                    });
                }
                return;
            }

            if (newPage) {
                context.commit("setPage", newPage);
            }

            if (context.state.loading) {
                //return if previous response is still in progress
                return;
            }

            context.commit("setLoading", true);

            const startSearchQuery = context.state.query;
            const response = await searchPackages(context.state.query, context.state.page - 1, 10);
            context.commit("setPagesCount", response.response.nbPages - 1);
            context.commit("setPackages", response.response.hits);
            context.commit("setLoading", false);

            if (startSearchQuery != context.state.query) {
                //if query changed during server response start search again
                context.dispatch("searchPackages");
            } else {
                //update routing according search results
                if (router.currentRoute.params.page != context.state.page ||
                    decodeURI(router.currentRoute.params.query) !== context.state.query) {
                    router.push({
                        name: 'search',
                        params: {page: context.state.page, query: context.state.query}
                    });
                }
            }
        }
    },
    modules: {}
})




