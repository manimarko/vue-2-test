import Vue from 'vue'
import App from './App.vue'
import store from './store'
import vuetify from './plugins/vuetify';
import VuetifyDialog from "vuetify-dialog";
import {router} from "./route";

Vue.config.productionTip = false

Vue.use(VuetifyDialog, {
    context: {
        vuetify
    }
})

new Vue({
    store,
    vuetify,
    router,
    render: h => h(App)
}).$mount('#app')
