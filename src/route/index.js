import Vue from "vue";
import VueRouter from "vue-router";
import App from "../App";

Vue.use(VueRouter);

export const router = new VueRouter({
    mode: 'history',
    base: "./",
    routes: [
        { path: "/", name:"home", component: App},
        { path: "/:query/:page", name:"search", component: App},
    ]
})